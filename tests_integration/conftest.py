import pytest
from sqlalchemy.ext.asyncio import async_sessionmaker, create_async_engine

from test_data.models import Base


def pytest_addoption(parser):
    parser.addoption('--dburl',
                     action='store',
                     default=None,
                     help='url of the database to use for tests')


@pytest.fixture(scope="session")
def db_url(request, tmp_path_factory):
    db_url = request.config.getoption("--dburl")
    if db_url is None:
        filename = tmp_path_factory.mktemp("sqlite") / "test.sqlite"
        db_url = "sqlite+aiosqlite:///" + str(filename)
    return db_url


@pytest.fixture(scope='session')
async def async_db_engine(db_url):
    """yields a SQLAlchemy engine which is suppressed after the test session"""
    engine_ = create_async_engine(db_url, echo=True)
    async with engine_.begin() as conn:
        await conn.run_sync(Base.metadata.create_all)
    yield engine_

    await engine_.dispose()


@pytest.fixture(scope='session')
def async_db_session_factory(async_db_engine):
    """returns a SQLAlchemy scoped session factory"""
    return async_sessionmaker(autocommit=False, autoflush=False, expire_on_commit=False, bind=async_db_engine)


@pytest.fixture(scope='function')
async def async_db_session(async_db_session_factory):
    """
    yields a SQLAlchemy connection which is rollbacked after the test
    a new sqlite database is generated each time if db_url is left as default so a clean db for each test
    """
    async with async_db_session_factory() as session_:
        yield session_
        await session_.rollback()
        await session_.close()
