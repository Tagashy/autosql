import pytest

from sqlgen.repository import AsyncObjectBoundRepository
from sqlgen.exc import BoundObjectLinkNotSafe
from test_data.factories import ProjectFactory, RequestFactory
from test_data.models import Request, Project


async def test_get_all_should_return_filtered_list_of_request(async_db_session):
    class ProjectRelatedRepository(AsyncObjectBoundRepository):
        bound_model = Project

    class RequestRepository(ProjectRelatedRepository):
        cls = Request

    project = ProjectFactory(hosts=[])
    request = RequestFactory(webserver__host__project=project)
    async_db_session.add(request)
    async_db_session.add(RequestFactory())
    await async_db_session.flush()
    repository = RequestRepository(async_db_session, project.id)

    # when
    new_request = await repository.create(webserver_id=request.webserver_id)
    results = await repository.get_all()

    # then
    assert results == [request , new_request]
async def test_create_should_fail_for_other_project_webserver(async_db_session):
    class ProjectRelatedRepository(AsyncObjectBoundRepository):
        bound_model = Project

    class RequestRepository(ProjectRelatedRepository):
        cls = Request

    project = ProjectFactory(hosts=[])
    request = RequestFactory() # not the same project
    async_db_session.add(request)
    async_db_session.add(project)
    async_db_session.add(RequestFactory())
    await async_db_session.flush()

    repository = RequestRepository(async_db_session, project.id)

    # then
    with pytest.raises(BoundObjectLinkNotSafe):
        # when
        await repository.create(webserver_id=request.webserver_id)

