import pytest

from sqlgen.repository.impl.async_constrained import AsyncConstrainedRepository
from sqlgen.exc import ConstraintNotSafe
from test_data.factories import ProjectFactory, RequestFactory
from test_data.models import Request, Project, Webserver


async def test_get_all_should_filter_on_project_id_and_webserver_id(async_db_session):
    class ProjectRelatedRepository(AsyncConstrainedRepository):
        bound_model = Project

    class WebserverRelatedRepository(AsyncConstrainedRepository):
        bound_model = Webserver

    class RequestRepository(ProjectRelatedRepository, WebserverRelatedRepository):
        cls = Request

    project = ProjectFactory(hosts=[])
    request = RequestFactory(webserver__host__project=project)
    other_webserver_request_within_project = RequestFactory(webserver__host__project=project)
    async_db_session.add(other_webserver_request_within_project)
    async_db_session.add(request)
    async_db_session.add(RequestFactory())
    await async_db_session.flush()
    repository = RequestRepository(async_db_session, project_id=project.id, webserver_id=request.webserver_id)

    # when
    new_request = await repository.create()
    results = await repository.get_all()

    # then
    assert results == [request, new_request]


async def test_create_should_fail_for_other_project_webserver(async_db_session):
    class ProjectRelatedRepository(AsyncConstrainedRepository):
        bound_model = Project

    class WebserverRelatedRepository(AsyncConstrainedRepository):
        bound_model = Webserver

    class RequestRepository(ProjectRelatedRepository, WebserverRelatedRepository):
        cls = Request

    project = ProjectFactory(hosts=[])
    request = RequestFactory()  # not the same project
    async_db_session.add(request)
    async_db_session.add(project)
    async_db_session.add(RequestFactory())
    await async_db_session.flush()

    repository = RequestRepository(async_db_session, project_id=project.id, webserver_id=request.webserver_id)

    # then
    with pytest.raises(ConstraintNotSafe):
        # when
        await repository.create()
