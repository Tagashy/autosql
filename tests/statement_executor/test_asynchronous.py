import pytest
from sqlalchemy import Select, Result, ScalarResult
from sqlalchemy.ext.asyncio import AsyncSession

from sqlgen.statement_executor.asynchronous import AsyncStatementExecutor
from test_data.factories import ProjectFactory
from test_data.models import Project


@pytest.fixture()
def async_statement_executor(mocker):
    return AsyncStatementExecutor(mocker.Mock(spec=AsyncSession))


async def test_execute_statement_should_return_session_execute(async_statement_executor):
    # given
    statement = Select(Project)
    # when
    result = await async_statement_executor.execute_statement(statement)
    # then
    assert result == async_statement_executor.session.execute.return_value
    async_statement_executor.session.execute.assert_called_once_with(statement)


async def test_scalars_should_call_execute(async_statement_executor, mocker):
    statement = Select(Project)
    execute_statement_patch = mocker.patch.object(async_statement_executor, "execute_statement",
                                                  return_value=mocker.Mock(spec=Result))
    # when
    result = await async_statement_executor.scalars(statement)
    # then
    assert result == execute_statement_patch.return_value.unique.return_value.scalars.return_value
    execute_statement_patch.assert_called_once_with(statement)


async def test_one_should_call_scalars(async_statement_executor, mocker):
    statement = Select(Project)
    scalars_patch = mocker.patch.object(async_statement_executor, "scalars",
                                        return_value=mocker.Mock(spec=ScalarResult))
    # when
    result = await async_statement_executor.one(statement)
    # then
    assert result == scalars_patch.return_value.one.return_value
    scalars_patch.assert_called_once_with(statement)


async def test_one_or_none_should_call_scalars(async_statement_executor, mocker):
    statement = Select(Project)
    scalars_patch = mocker.patch.object(async_statement_executor, "scalars",
                                        return_value=mocker.Mock(spec=ScalarResult))
    # when
    result = await async_statement_executor.one_or_none(statement)
    # then
    assert result == scalars_patch.return_value.one_or_none.return_value
    scalars_patch.assert_called_once_with(statement)


async def test_all_should_call_scalars(async_statement_executor, mocker):
    statement = Select(Project)
    scalars_patch = mocker.patch.object(async_statement_executor, "scalars",
                                        return_value=mocker.Mock(spec=ScalarResult))
    # when
    result = await async_statement_executor.all(statement)
    # then
    assert result == scalars_patch.return_value.all.return_value
    scalars_patch.assert_called_once_with(statement)


async def test_synchronize_should_call_session_flush(async_statement_executor):
    # when
    await async_statement_executor.synchronize()
    # then
    async_statement_executor.session.flush.assert_called_once_with()


async def test_save_should_call_session_commit(async_statement_executor):
    # when
    await async_statement_executor.save()
    # then
    async_statement_executor.session.commit.assert_called_once_with()


async def test_restore_should_call_session_rollback(async_statement_executor):
    # when
    await async_statement_executor.restore()
    # then
    async_statement_executor.session.rollback.assert_called_once_with()


def test_store_should_call_session_add(async_statement_executor):
    project = ProjectFactory()
    # when
    async_statement_executor.store(project)
    # then
    async_statement_executor.session.add.assert_called_once_with(project)
