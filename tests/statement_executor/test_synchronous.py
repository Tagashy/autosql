from unittest.mock import Mock

import pytest
from sqlalchemy import Select, ScalarResult, Result
from sqlalchemy.orm import Session

from sqlgen.statement_executor.synchronous import SynchronousStatementExecutor
from test_data.factories import ProjectFactory
from test_data.models import Project


@pytest.fixture
def synchronous_statement_executor():
    return SynchronousStatementExecutor(Mock(spec=Session))


def test_execute_statement_should_return_session_execute(synchronous_statement_executor):
    # given
    statement = Select(Project)
    # when
    result = synchronous_statement_executor.execute_statement(statement)
    # then
    assert result == synchronous_statement_executor.session.execute.return_value
    synchronous_statement_executor.session.execute.assert_called_once_with(statement)


def test_scalars_should_call_execute(synchronous_statement_executor, mocker):
    statement = Select(Project)
    execute_statement_patch = mocker.patch.object(synchronous_statement_executor, "execute_statement",
                                                  return_value=mocker.Mock(spec=Result))
    # when
    result = synchronous_statement_executor.scalars(statement)
    # then
    assert result == execute_statement_patch.return_value.unique.return_value.scalars.return_value
    execute_statement_patch.assert_called_once_with(statement)


def test_one_should_call_scalars(synchronous_statement_executor, mocker):
    statement = Select(Project)
    scalars_patch = mocker.patch.object(synchronous_statement_executor, "scalars",
                                        return_value=mocker.Mock(spec=ScalarResult))
    # when
    result = synchronous_statement_executor.one(statement)
    # then
    assert result == scalars_patch.return_value.one.return_value
    scalars_patch.assert_called_once_with(statement)


def test_one_or_none_should_call_scalars(synchronous_statement_executor, mocker):
    statement = Select(Project)
    scalars_patch = mocker.patch.object(synchronous_statement_executor, "scalars",
                                        return_value=mocker.Mock(spec=ScalarResult))
    # when
    result = synchronous_statement_executor.one_or_none(statement)
    # then
    assert result == scalars_patch.return_value.one_or_none.return_value
    scalars_patch.assert_called_once_with(statement)


def test_all_should_call_scalars(synchronous_statement_executor, mocker):
    statement = Select(Project)
    scalars_patch = mocker.patch.object(synchronous_statement_executor, "scalars",
                                        return_value=mocker.Mock(spec=ScalarResult))
    # when
    result = synchronous_statement_executor.all(statement)
    # then
    assert result == scalars_patch.return_value.all.return_value
    scalars_patch.assert_called_once_with(statement)


def test_synchronize_should_call_session_flush(synchronous_statement_executor):
    # when
    synchronous_statement_executor.synchronize()
    # then
    synchronous_statement_executor.session.flush.assert_called_once_with()


def test_save_should_call_session_commit(synchronous_statement_executor):
    # when
    synchronous_statement_executor.save()
    # then
    synchronous_statement_executor.session.commit.assert_called_once_with()


def test_restore_should_call_session_rollback(synchronous_statement_executor):
    # when
    synchronous_statement_executor.restore()
    # then
    synchronous_statement_executor.session.rollback.assert_called_once_with()


def test_store_should_call_session_add(synchronous_statement_executor):
    project = ProjectFactory()
    # when
    synchronous_statement_executor.store(project)
    # then
    synchronous_statement_executor.session.add.assert_called_once_with(project)
