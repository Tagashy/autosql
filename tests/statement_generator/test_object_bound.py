from uuid import uuid4

import pytest
from sqlalchemy import Select

from sqlgen.exc import BoundObjectLinkNotSafe, ForeignKeyNotSpecified
from sqlgen.statement_generator.object_bound import ObjectBoundStatementGenerator
from test_data.factories import ProjectFactory
from test_data.models import Host, Webserver


@pytest.fixture()
def project():
    return ProjectFactory()


@pytest.fixture()
def object_bound_statement_generator(project):
    class HostStatementGenerator(ObjectBoundStatementGenerator):
        cls = Host
        joins = [Host.project_id]

    return HostStatementGenerator(project.id)


@pytest.fixture()
def webserver_object_bound_statement_generator(project):
    class WebserverStatementGenerator(ObjectBoundStatementGenerator):
        cls = Webserver
        joins = [Webserver.host, Host.project_id]

    return WebserverStatementGenerator(project.id)


def test_get_by_should_return_filtered_query_by_bound_object(object_bound_statement_generator, project):
    # given
    expected_query = Select(Host).filter(Host.project_id == project.id)
    # when
    result = object_bound_statement_generator.get_by()
    # then
    assert str(result) == str(expected_query)


def test_get_by_should_return_filtered_query_by_bound_object_with_joins(webserver_object_bound_statement_generator,
                                                                        project):
    # given
    expected_query = Select(Webserver).join(Webserver.host).filter(Host.project_id == project.id)
    # when
    result = webserver_object_bound_statement_generator.get_by()
    # then
    assert str(result) == str(expected_query)


def test_create_should_add_bound_project_id_to_created_object(object_bound_statement_generator, project):
    # given

    # when
    result = object_bound_statement_generator.create(name="toto")
    # then
    assert isinstance(result, Host)
    assert result.project_id == project.id
    assert result.name == "toto"


def test_create_should_add_raise_if_relation_is_not_specified(project):
    # given
    class TestStatementGenerator(ObjectBoundStatementGenerator):
        cls = Webserver
        joins = [Webserver.host, Host.project_id]

    object_bound_statement_generator = TestStatementGenerator(project.id)
    # then
    with pytest.raises(ForeignKeyNotSpecified) as exc_info:
        # when
        object_bound_statement_generator.create(name="toto")
    assert exc_info.value.foreign_key == "host_id"
    assert str(exc_info.value) == ("Cannot return create args due to multiple joins between created model and "
                                   "bound model. To fix this error, please specify the first join relation of the "
                                   f"chain in the parameters (host_id). "
                                   f"e.g: repository.create(host_id=123, your_arguments)."
                                   "if you have an idea on how to resolve this properly make a "
                                   "suggestion to the dev i'll happily listen to it.")


def test_create_should_raise_if_relation_is_specified_but_safe_param_is_not(object_bound_statement_generator, project):
    # given

    class TestStatementGenerator(ObjectBoundStatementGenerator):
        cls = Webserver
        joins = [Webserver.host, Host.project_id]

    object_bound_statement_generator = TestStatementGenerator(project.id)

    # then
    with pytest.raises(BoundObjectLinkNotSafe):
        # when
        object_bound_statement_generator.create(host_id=uuid4(), name="toto")


def test_create_should_return_obj_if_relation_is_safe(object_bound_statement_generator, project):
    # given

    class TestStatementGenerator(ObjectBoundStatementGenerator):
        cls = Webserver
        joins = [Webserver.host, Host.project_id]

    object_bound_statement_generator = TestStatementGenerator(project.id)
    host_id = uuid4()
    # when
    result = object_bound_statement_generator.create(host_id=host_id, name="toto", safe=True)
    # then
    assert isinstance(result, Webserver)
    assert result.host_id == host_id
    assert result.name == "toto"


def test_create_should_raise_if_inconsistent_state(object_bound_statement_generator, project):
    # given

    class TestStatementGenerator(ObjectBoundStatementGenerator):
        cls = Webserver
        joins = []

    object_bound_statement_generator = TestStatementGenerator(project.id)
    host_id = uuid4()
    # then
    with pytest.raises(NotImplementedError):
        # when
        object_bound_statement_generator.create(host_id=host_id, name="toto")
