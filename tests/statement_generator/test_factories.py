from uuid import uuid4

from sqlgen.statement_generator.base import StatementGenerator
from sqlgen.statement_generator.constrained import ConstrainedStatementGenerator
from sqlgen.joins import Constraint
from sqlgen.statement_generator.factories import make_statement_generator_class_for, \
    make_object_bound_statement_generator_class_for, make_constrained_statement_generator_class_for
from sqlgen.statement_generator.object_bound import ObjectBoundStatementGenerator
from test_data.models import Project, Request, Webserver, Host


def test_make_statement_generator_class_for_should_return_a_statement_generator_class():
    statement_generator_class = make_statement_generator_class_for(Project)
    # then
    assert isinstance(statement_generator_class(), StatementGenerator)
    assert statement_generator_class.cls == Project


def test_make_object_bound_statement_generator_class_for_should_return_a_statement_generator_class():
    # given
    project_id = uuid4()
    # when
    statement_generator_class = make_object_bound_statement_generator_class_for(Request, Project)
    # then
    assert isinstance(statement_generator_class(project_id), ObjectBoundStatementGenerator)
    assert statement_generator_class.cls == Request
    assert statement_generator_class.joins == [Request.webserver, Webserver.host, Host.project_id]


def test_make_constrained_statement_generator_class_for_should_return_a_statement_generator_class():
    # given
    project_id = uuid4()
    webserver_id = uuid4()
    # when
    statement_generator_class = make_constrained_statement_generator_class_for(Request, [Project, Webserver])
    # then
    assert isinstance(statement_generator_class(project_id=project_id, webserver_id=webserver_id),
                      ConstrainedStatementGenerator)
    assert statement_generator_class.cls == Request
    assert statement_generator_class.constraints == [
        Constraint(Host.project_id, Project, [Request.webserver, Webserver.host], _bound_object_id=project_id),
        Constraint(Request.webserver_id, Webserver, [], _bound_object_id=webserver_id),
    ]
