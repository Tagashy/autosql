from uuid import uuid4

import pytest
from sqlalchemy import Select
from sqlalchemy.orm import joinedload, defer

from sqlgen.statement_generator.base import StatementGenerator
from test_data.models import Host


@pytest.fixture()
def statement_generator():
    class HostStatementGenerator(StatementGenerator):
        cls = Host

    return HostStatementGenerator()


def test_get_by_should_return_select_with_no_filter(statement_generator):
    # given
    expected_query = Select(Host)
    # when
    result = statement_generator.get_by()
    # then
    assert str(result) == str(expected_query)


def test_get_by_should_return_select_property(statement_generator):
    # given
    expected_query = Select(Host.project_id)
    # when
    result = statement_generator.get_by(property_name="project_id")
    # then
    assert str(result) == str(expected_query)


def test_get_by_should_return_filtered_query(statement_generator):
    # given
    project_id = uuid4()
    expected_query = Select(Host).filter_by(project_id=project_id)
    # when
    result = statement_generator.get_by(project_id=project_id)
    # then
    assert str(result) == str(expected_query)


def test_get_by_should_return_filtered_query_with_custom_filter(statement_generator):
    # given
    project_id = uuid4()
    expected_query = Select(Host).filter(Host.project_id.in_([project_id]))
    # when
    result = statement_generator.get_by(Host.project_id.in_([project_id]))
    # then
    assert str(result) == str(expected_query)


def test_get_by_should_return_query_with_join_all(statement_generator):
    # given
    expected_query = Select(Host).options(joinedload(Host.project), joinedload(Host.webservers))
    # when
    result = statement_generator.get_by(load_all=True)
    # then
    assert str(result) == str(expected_query)


def test_get_by_should_return_query_with_join_all_and_custom_options(statement_generator):
    # given
    expected_query = Select(Host).options(defer(Host.name), joinedload(Host.project), joinedload(Host.webservers))
    # when
    result = statement_generator.get_by(load_all=True, options=[defer(Host.name)])
    # then
    assert str(result) == str(expected_query)


def test_get_by_id_should_call_get_by_with_id_specified(statement_generator, mocker):
    # given
    project_id = uuid4()
    get_by_patch = mocker.patch.object(statement_generator, "get_by")
    filter_ = Host.project_id.in_([project_id])
    # when
    statement_generator.get_by_id(project_id, filter_, name="test")
    # then
    get_by_patch.assert_called_once_with(filter_, name="test", id=project_id)
