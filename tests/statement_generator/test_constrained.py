from uuid import uuid4

import pytest
from sqlalchemy import Select

from sqlgen.exc import ConstraintNotSafe, DirectConstraintNotSafe, ForeignKeyNotSpecified, ConstraintUninitialized, \
    MissingKeywordArgument
from sqlgen.joins import NoValidJoins, Constraint
from sqlgen.statement_generator.constrained import ConstrainedStatementGenerator, build_parent_constraint
from test_data.factories import ProjectFactory, HostFactory, WebserverFactory
from test_data.models import Host, Webserver, Project, Request, VulnerabilityInstance


@pytest.fixture()
def project():
    return ProjectFactory()


@pytest.fixture()
def host(project):
    return HostFactory(project=project)


@pytest.fixture()
def webserver(host):
    return WebserverFactory(host=host)


@pytest.fixture()
def single_constraint_statement_generator(project):
    class SingleConstraintStatementGenerator(ConstrainedStatementGenerator):
        cls = Host
        constraints = [Constraint(Host.project_id, Project)]

    return SingleConstraintStatementGenerator(project_id=project.id)


@pytest.fixture()
def dual_constraint_statement_generator(project, host):
    class SingleConstraintStatementGenerator(ConstrainedStatementGenerator):
        cls = Webserver
        constraints = [
            Constraint(Host.project_id, Project, joins=[Webserver.host]),
            Constraint(Webserver.host_id, Host),
        ]

    return SingleConstraintStatementGenerator(project_id=project.id, host_id=host.id)


@pytest.fixture()
def triple_constraint_statement_generator(project, host, webserver):
    class SingleConstraintStatementGenerator(ConstrainedStatementGenerator):
        cls = Request
        constraints = [
            Constraint(Host.project_id, Project, joins=[Request.webserver, Webserver.host]),
            Constraint(Webserver.host_id, Host, joins=[Request.webserver]),
            Constraint(Request.webserver_id, Webserver)
        ]

    return SingleConstraintStatementGenerator(project_id=project.id, host_id=host.id, webserver_id=webserver.id)


def test_get_by_should_return_filtered_query_by_bound_object(single_constraint_statement_generator, project):
    # given
    expected_query = Select(Host).filter(Host.project_id == project.id)
    # when
    result = single_constraint_statement_generator.get_by()
    # then
    assert str(result) == str(expected_query)


def test_get_by_should_return_filtered_query_by_bound_object_with_joins(dual_constraint_statement_generator,
                                                                        project, host):
    # given
    expected_query = Select(Webserver).join(Webserver.host).filter(Host.project_id == project.id,
                                                                   Webserver.host_id == host.id)
    # when
    result = dual_constraint_statement_generator.get_by()
    # then
    assert str(result) == str(expected_query)


def test_get_by_should_return_filtered_query_with_three_constraint(triple_constraint_statement_generator,
                                                                   project, host, webserver):
    # given
    expected_query = Select(Request).join(Request.webserver).join(Webserver.host).filter(Host.project_id == project.id,
                                                                                         Webserver.host_id == host.id,
                                                                                         Request.webserver_id == webserver.id)
    # when
    result = triple_constraint_statement_generator.get_by()
    # then
    assert str(result) == str(expected_query)


def test_validate_constraint_should_return_foreign_key_name_and_value(single_constraint_statement_generator, project):
    # given
    constraint = single_constraint_statement_generator.constraints[0]
    # when
    result = single_constraint_statement_generator.validate_constraint(constraint, [])
    # then
    assert result == {"project_id": project.id}


def test_validate_constraint_should_return_empty_dict_if_constraint_has_been_validated(
        dual_constraint_statement_generator, project):
    # given
    constraint = dual_constraint_statement_generator.constraints[0]
    # when
    result = dual_constraint_statement_generator.validate_constraint(constraint, [constraint])
    # then
    assert result == {}


def test_validate_constraint_should_raise_foreign_key_not_specified(project):
    # given
    constraint = Constraint(Host.project_id, Project, joins=[Request.webserver, Webserver.host])

    class SingleConstraintStatementGenerator(ConstrainedStatementGenerator):
        cls = Request
        constraints = [constraint]

    statement_generator = SingleConstraintStatementGenerator(project_id=project.id)
    # then
    with pytest.raises(ForeignKeyNotSpecified):
        # when
        statement_generator.validate_constraint(constraint, [])


def test_validate_constraint_should_raise_bound_object_link_not_safe_with_only_one_constraint(webserver, project):
    # given
    constraint = Constraint(Host.project_id, Project, joins=[Request.webserver, Webserver.host])

    class SingleConstraintStatementGenerator(ConstrainedStatementGenerator):
        cls = Request
        constraints = [constraint]

    statement_generator = SingleConstraintStatementGenerator(project_id=project.id)
    # then
    with pytest.raises(DirectConstraintNotSafe) as exc_info:
        # when
        statement_generator.validate_constraint(constraint, [], webserver_id=webserver.id)

    assert exc_info.value.foreign_key_value == webserver.id
    assert exc_info.value.model == Webserver


def test_validate_constraint_should_raise_constraint_not_safe_if_a_constraint_provide_foreign_key(
        dual_constraint_statement_generator, host):
    # given
    constraint = dual_constraint_statement_generator.constraints[0]
    # then
    with pytest.raises(ConstraintNotSafe):
        # when
        dual_constraint_statement_generator.validate_constraint(constraint, [])


def test_validate_constraint_should_raise_constraint_not_safe_if_a_constraint_provide_foreign_key_three_constraint(
        triple_constraint_statement_generator, host):
    # given
    constraint = triple_constraint_statement_generator.constraints[0]
    # then
    with pytest.raises(ConstraintNotSafe):
        # when
        triple_constraint_statement_generator.validate_constraint(constraint, [])


def test_validate_constraint_should_raise_constraint_not_safe_even_if_foreign_key_is_provided(
        # this is not a supported scenario
        dual_constraint_statement_generator, host):
    # given
    constraint = dual_constraint_statement_generator.constraints[0]
    # then
    with pytest.raises(ConstraintNotSafe):
        # when
        dual_constraint_statement_generator.validate_constraint(constraint, [], host_id=host.id)


def test_create_should_add_bound_project_id_to_created_object(single_constraint_statement_generator, project):
    # given

    # when
    result = single_constraint_statement_generator.create(name="toto")
    # then
    assert isinstance(result, Host)
    assert result.project_id == project.id
    assert result.name == "toto"


def test_create_should_add_bound_host_id(dual_constraint_statement_generator, project, host):
    # given

    # when
    result = dual_constraint_statement_generator.create(name="toto", safe_constraints=[
        dual_constraint_statement_generator.constraints[0]])
    # then
    assert isinstance(result, Webserver)
    assert result.host_id == host.id
    assert result.name == "toto"


def test_create_should_add_webserver_id(triple_constraint_statement_generator, project, host, webserver):
    # given

    # when
    result = triple_constraint_statement_generator.create(flag=True,
                                                          safe_constraints=triple_constraint_statement_generator.constraints[
                                                                           :-1])
    # then
    assert isinstance(result, Request)
    assert result.webserver_id == webserver.id
    assert result.flag is True


def test_constraint_should_raise_if_bound_object_id_is_accessed_before_initialized():
    # given
    constraint = Constraint(Host.project_id, Project, joins=[Request.webserver, Webserver.host])
    # then
    with pytest.raises(ConstraintUninitialized):
        # when
        print(constraint.bound_object_id)


def test_init_should_raise_if_bound_object_id_is_not_specified():
    class SingleConstraintStatementGenerator(ConstrainedStatementGenerator):
        cls = Host
        constraints = [Constraint(Host.project_id, Project)]

    with pytest.raises(MissingKeywordArgument) as exc_info:  # missing foreign_key
        SingleConstraintStatementGenerator()
    assert exc_info.value.argument_name == "project_id"


@pytest.mark.parametrize("constraint,model,expected_constraint", [
    (
            Constraint(Host.project_id, Project,
                       joins=[VulnerabilityInstance.request, Request.webserver, Webserver.host],
                       _bound_object_id=(project_id := uuid4())),
            Webserver,
            Constraint(Host.project_id, Project, joins=[Webserver.host], _bound_object_id=project_id)
    ),  # expect 2 cuts within joins and still having joins not empty
    (
            Constraint(Host.project_id, Project, joins=[Request.webserver, Webserver.host],
                       _bound_object_id=(project_id := uuid4())),
            Webserver,
            Constraint(Host.project_id, Project, joins=[Webserver.host], _bound_object_id=project_id)
    ),  # expect 1 cut within joins and still having joins not empty
    (
            Constraint(Host.project_id, Project, joins=[Request.webserver, Webserver.host],
                       _bound_object_id=(project_id := uuid4())),
            Host,
            Constraint(Host.project_id, Project, joins=[], _bound_object_id=project_id)
    ),  # expect 2 cuts within joins and having joins  empty
    (
            Constraint(Host.project_id, Project, joins=[Webserver.host], _bound_object_id=(project_id := uuid4())),
            Host,
            Constraint(Host.project_id, Project, joins=[], _bound_object_id=project_id)
    ),  # expect 1 cut within joins and having joins empty

])
def test_build_parent_constraint_should_return_filtered_constraint(constraint, model, expected_constraint):
    # when
    result = build_parent_constraint(constraint, model)
    # then
    assert result == expected_constraint


def test_build_parent_constraint_should_raise_if_model_to_build_constraint_for_is_not_linked():
    # given
    constraint = Constraint(Host.project_id, Project, joins=[Webserver.host], _bound_object_id=uuid4())
    model = VulnerabilityInstance
    # then
    with pytest.raises(NoValidJoins):
        # when
        build_parent_constraint(constraint, model)
