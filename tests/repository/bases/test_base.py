import pytest

from sqlgen.repository.bases.database_repository import DatabaseRepository
from sqlgen.statement_generator.base import StatementGenerator
from test_data.models import Request


async def test_database_repository_should_raise_if_instantiated_directly():
    with pytest.raises(ValueError) as exc_info:
        DatabaseRepository()
    # then
    assert exc_info.value.args == ("Cannot instantiate DatabaseRepository directly",)


def test_database_repository_should_generate_statement_factory_class():
    # given
    class TestRepository(DatabaseRepository):
        cls = Request

    # when
    repository = TestRepository()

    # then
    assert TestRepository.statement_generator_factory.cls == Request
    assert repository.statement_generator.cls == Request
    assert isinstance(repository.statement_generator, StatementGenerator)
