from uuid import uuid4

import pytest

from sqlgen.exc import NoBoundModelDefined
from sqlgen.repository.bases.object_bound import ObjectBoundRepository
from sqlgen.statement_generator.object_bound import ObjectBoundStatementGenerator
from test_data.models import Request, Project, Webserver, Host


async def test_object_bound_repository_should_raise_if_instantiated_directly():
    with pytest.raises(ValueError) as exc_info:
        ObjectBoundRepository(uuid4())
    # then
    assert exc_info.value.args == ("Cannot instantiate ObjectBoundRepository directly",)


async def test_init_should_generate_appropriate_statement_generator():
    class ProjectBoundRequestRepository(ObjectBoundRepository):
        cls = Request
        bound_model = Project

    project_id = uuid4()
    # when
    repository = ProjectBoundRequestRepository(project_id)
    # then
    assert isinstance(repository.statement_generator, ObjectBoundStatementGenerator)
    assert repository.statement_generator.joins == [Request.webserver, Webserver.host, Host.project_id]


async def test_init_should_generate_appropriate_statement_generator_for_child_class():
    class ProjectRelatedRepository[T](ObjectBoundRepository):
        bound_model = Project

    class RequestRepository(ProjectRelatedRepository):
        cls = Request

    project_id = uuid4()
    # when
    repository = RequestRepository(project_id)
    # then
    assert isinstance(repository.statement_generator, ObjectBoundStatementGenerator)
    assert repository.statement_generator.joins == [Request.webserver, Webserver.host, Host.project_id]


def test_object_bound_repository_should_raise_when_trying_to_generate_without_constraints():
    # then
    with pytest.raises(NoBoundModelDefined):
        # when
        class MyRepository(ObjectBoundRepository):
            cls = Request
