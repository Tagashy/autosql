import pytest
from pytest_unordered import unordered

from sqlgen.exc import NoBoundModelDefined
from sqlgen.repository.bases.constrained import ConstrainedRepository
from test_data.models import Request, Project, Webserver

MODULE_PACKAGE = "sqlgen.repository.bases.constrained"


def test_constrained_repository_meta_should_generate_a_constrained_statement_generator_class(mocker):
    make_constrained_statement_generator_class_for_patch = mocker.patch(
        f"{MODULE_PACKAGE}.make_constrained_statement_generator_class_for")

    # when
    class MyRepository(ConstrainedRepository):
        cls = Request
        bound_model = Project

    # then
    make_constrained_statement_generator_class_for_patch.assert_called_once_with(Request, [Project])


def test_constrained_repository_meta_should_generate_a_constrained_statement_generator_class_with_one_parent(mocker):
    make_constrained_statement_generator_class_for_patch = mocker.patch(
        f"{MODULE_PACKAGE}.make_constrained_statement_generator_class_for")

    # when
    class BaseRepository(ConstrainedRepository):
        bound_model = Project

    class MyRepository(BaseRepository):
        cls = Request

    # then
    make_constrained_statement_generator_class_for_patch.assert_called_once_with(Request, [Project])


def test_constrained_repository_meta_should_generate_a_constrained_statement_generator_class_with_two_parents(mocker):
    make_constrained_statement_generator_class_for_patch = mocker.patch(
        f"{MODULE_PACKAGE}.make_constrained_statement_generator_class_for")

    # when
    class ProjectBoundRepository(ConstrainedRepository):
        bound_model = Project

    class WebserverBoundRepository(ConstrainedRepository):
        bound_model = Webserver

    class MyRepository(ProjectBoundRepository, WebserverBoundRepository):
        cls = Request

    # then
    make_constrained_statement_generator_class_for_patch.assert_called_once_with(Request,
                                                                                 unordered([Project, Webserver]))


def test_constrained_repository_meta_should_work_with_unrelated_class_heritage(mocker):
    make_constrained_statement_generator_class_for_patch = mocker.patch(
        f"{MODULE_PACKAGE}.make_constrained_statement_generator_class_for")

    # when
    class ProjectBoundRepository(ConstrainedRepository):
        bound_model = Project

    class FakeMixin:
        pass

    class WebserverBoundRepository(ConstrainedRepository):
        bound_model = Webserver

    class MyRepository(ProjectBoundRepository, WebserverBoundRepository, FakeMixin):
        cls = Request

    # then
    make_constrained_statement_generator_class_for_patch.assert_called_once_with(Request,
                                                                                 unordered([Project, Webserver]))


def test_constrained_repository_meta_should_use_bound_models(mocker):
    make_constrained_statement_generator_class_for_patch = mocker.patch(
        f"{MODULE_PACKAGE}.make_constrained_statement_generator_class_for")

    # when
    class MyRepository(ConstrainedRepository):
        cls = Request
        bound_models = [Project, Webserver]

    # then
    make_constrained_statement_generator_class_for_patch.assert_called_once_with(Request,
                                                                                 unordered([Project, Webserver]))


def test_constrained_repository_should_raise_when_trying_to_generate_without_constraints():
    # then
    with pytest.raises(NoBoundModelDefined):
        # when
        class MyRepository(ConstrainedRepository):
            cls = Request
