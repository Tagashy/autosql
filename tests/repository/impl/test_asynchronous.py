from uuid import uuid4

import pytest
from sqlalchemy.ext.asyncio import AsyncSession

from sqlgen.repository.impl.asynchronous import AsyncRepository
from test_data.factories import HostFactory
from test_data.models import Host
from tests.matchers import InstanceOfWith


@pytest.fixture()
def async_repository(mocker):
    class Repository(AsyncRepository):
        cls = Host

    return Repository(mocker.Mock(spec=AsyncSession))


async def test_async_repository_should_raise_if_instantiated_directly(mocker):
    with pytest.raises(ValueError) as exc_info:
        AsyncRepository(mocker.Mock(spec=AsyncSession), uuid4())
    # then
    assert exc_info.value.args == ("Cannot instantiate AsyncRepository directly",)


async def test_get_by_should_execute_statement_returned_by_statement_generator(async_repository, mocker):
    # given
    project_id = uuid4()
    get_by_patch = mocker.patch.object(async_repository.statement_generator, "get_by")
    one_or_none_patch = mocker.patch.object(async_repository.statement_executor, "one_or_none")
    filter_ = Host.project_id.in_([project_id])
    # when
    await async_repository.get_by(filter_, name="test")
    # then
    get_by_patch.assert_called_once_with(filter_, name="test", load_all=False, options=None,
                                         property_name=None)
    one_or_none_patch.assert_called_once_with(get_by_patch.return_value)


async def test_take_by_should_execute_statement_returned_by_statement_generator(async_repository, mocker):
    # given
    project_id = uuid4()
    get_by_patch = mocker.patch.object(async_repository.statement_generator, "get_by")
    one_patch = mocker.patch.object(async_repository.statement_executor, "one")
    filter_ = Host.project_id.in_([project_id])
    # when
    await async_repository.take_by(filter_, name="test")
    # then
    get_by_patch.assert_called_once_with(filter_, name="test", load_all=False, options=None,
                                         property_name=None)
    one_patch.assert_called_once_with(get_by_patch.return_value)


async def test_get_by_id_should_execute_statement_returned_by_statement_generator(async_repository, mocker):
    # given
    project_id = uuid4()
    get_by_patch = mocker.patch.object(async_repository.statement_generator, "get_by")
    one_or_none_patch = mocker.patch.object(async_repository.statement_executor, "one_or_none")
    filter_ = Host.project_id.in_([project_id])
    # when
    await async_repository.get_by_id(project_id, filter_, name="test")
    # then
    get_by_patch.assert_called_once_with(filter_, name="test", id=project_id, load_all=False, options=None,
                                         property_name=None)
    one_or_none_patch.assert_called_once_with(get_by_patch.return_value)


async def test_take_by_id_should_execute_statement_returned_by_statement_generator(async_repository, mocker):
    # given
    project_id = uuid4()
    get_by_patch = mocker.patch.object(async_repository.statement_generator, "get_by")
    one_patch = mocker.patch.object(async_repository.statement_executor, "one")
    filter_ = Host.project_id.in_([project_id])
    # when
    await async_repository.take_by_id(project_id, filter_, name="test")
    # then
    get_by_patch.assert_called_once_with(filter_, name="test", id=project_id, load_all=False, options=None,
                                         property_name=None)
    one_patch.assert_called_once_with(get_by_patch.return_value)


async def test_get_all_by_should_execute_statement_returned_by_statement_generator(async_repository, mocker):
    # given
    project_id = uuid4()
    get_by_patch = mocker.patch.object(async_repository.statement_generator, "get_by")
    all_patch = mocker.patch.object(async_repository.statement_executor, "all")
    filter_ = Host.project_id.in_([project_id])
    # when
    await async_repository.get_all_by(filter_, name="test")
    # then
    get_by_patch.assert_called_once_with(filter_, name="test", options=None)
    all_patch.assert_called_once_with(get_by_patch.return_value)


async def test_get_all_should_execute_statement_returned_by_statement_generator(async_repository, mocker):
    # given
    get_by_patch = mocker.patch.object(async_repository.statement_generator, "get_by")
    all_patch = mocker.patch.object(async_repository.statement_executor, "all")
    # when
    await async_repository.get_all()
    # then
    get_by_patch.assert_called_once_with(options=None)
    all_patch.assert_called_once_with(get_by_patch.return_value)


async def test_create_should_create_object_with(async_repository, mocker):
    # given
    project_id = uuid4()
    store_patch = mocker.patch.object(async_repository.statement_executor, "store")
    # when
    obj = await async_repository.create(name="toto", project_id=project_id)
    # then
    store_patch.assert_called_once_with(
        InstanceOfWith(Host, name="toto", project_id=project_id))
    assert isinstance(obj, Host)


async def test_update_should_set_properties_and_synchronize(async_repository, mocker):
    # given
    host = HostFactory()
    synchronize_patch = mocker.patch.object(async_repository, "synchronize")
    # when
    await async_repository.update(host, name="toto")
    # then
    assert host.name == "toto"
    synchronize_patch.assert_called_once_with()


async def test_update_should_commit(async_repository, mocker):
    # given
    host = HostFactory()
    save_patch = mocker.patch.object(async_repository, "save")
    # when
    await async_repository.update(host, save=True)
    # then
    save_patch.assert_called_once_with()


async def test_update_should_load_object_and_update_it(async_repository, mocker):
    # given
    host = HostFactory()
    take_by_id_patch = mocker.patch.object(async_repository, "take_by_id", return_value=host)
    # when
    await async_repository.update(host.id, name="toto")
    # then
    assert host.name == "toto"
    take_by_id_patch.assert_called_once_with(host.id, load_all=True)


async def test_synchronize_should_call_statement_executor_synchronize(async_repository, mocker):
    # given
    synchronize_patch = mocker.patch.object(async_repository.statement_executor, "synchronize")
    # when
    await async_repository.synchronize()
    # then
    synchronize_patch.assert_called_once_with()


async def test_save_should_call_statement_executor_save(async_repository, mocker):
    # given
    save_patch = mocker.patch.object(async_repository.statement_executor, "save")
    # when
    await async_repository.save()
    # then
    save_patch.assert_called_once_with()


async def test_restore_should_call_statement_executor_restore(async_repository, mocker):
    # given
    restore_patch = mocker.patch.object(async_repository.statement_executor, "restore")
    # when
    await async_repository.restore()
    # then
    restore_patch.assert_called_once_with()
