from uuid import uuid4

import pytest
from sqlalchemy.ext.asyncio import AsyncSession

from sqlgen.repository.impl.async_object_bound import AsyncObjectBoundRepository
from sqlgen.statement_executor.asynchronous import AsyncStatementExecutor
from sqlgen.exc import BoundObjectLinkNotSafe
from sqlgen.statement_generator.object_bound import ObjectBoundStatementGenerator
from test_data.factories import HostFactory, WebserverFactory
from test_data.models import Request, Host, Webserver


@pytest.fixture()
def host():
    return HostFactory()


@pytest.fixture()
def async_object_bound_repository(mocker, host):
    class Repository(AsyncObjectBoundRepository):
        cls = Request
        bound_model = Host

    return Repository(mocker.Mock(spec=AsyncSession), host.id)


async def test_async_object_bound_repository_should_present_an_init_function(mocker):
    # given
    class Test2(AsyncObjectBoundRepository):
        cls = Request
        bound_model = Host

    # when
    test = Test2(mocker.Mock(spec=AsyncSession), uuid4())
    # then
    assert isinstance(test.statement_generator, ObjectBoundStatementGenerator)
    assert isinstance(test.statement_executor, AsyncStatementExecutor)


async def test_async_object_bound_repository_should_raise_if_instantiated_directly(mocker):
    # given
    with pytest.raises(ValueError) as exc_info:
        # when
        AsyncObjectBoundRepository(mocker.Mock(spec=AsyncSession), uuid4())
    # then
    assert exc_info.value.args == ("Cannot instantiate AsyncObjectBoundRepository directly",)


async def test_create_should_return_object_if_valid(async_object_bound_repository, mocker):
    # given
    get_repository_for_patch = mocker.patch.object(async_object_bound_repository, "get_repository_for")
    get_repository_for_patch.return_value.get_by_id = mocker.AsyncMock(return_value=WebserverFactory())
    webserver_id = uuid4()
    # when
    result = await async_object_bound_repository.create(webserver_id=webserver_id)
    # then
    assert isinstance(result, Request)
    assert result.webserver_id == webserver_id


async def test_create_should_raise_if_link_is_unsafe(async_object_bound_repository, mocker):
    # given
    get_repository_for_patch = mocker.patch.object(async_object_bound_repository, "get_repository_for")
    get_repository_for_patch.return_value.get_by_id = mocker.AsyncMock(return_value=None)
    webserver_id = uuid4()
    # then
    with pytest.raises(BoundObjectLinkNotSafe) as exc_info:
        # when
        await async_object_bound_repository.create(webserver_id=webserver_id)
    assert exc_info.value.foreign_key_value == webserver_id
    assert exc_info.value.model == Webserver


async def test_create_should_raise_if_link_is_unsafe_but_safe_is_passed_as_arg(async_object_bound_repository, mocker):
    # given
    get_repository_for_patch = mocker.patch.object(async_object_bound_repository, "get_repository_for")
    get_repository_for_patch.return_value.get_by_id = mocker.AsyncMock(return_value=None)
    webserver_id = uuid4()
    # when
    result = await async_object_bound_repository.create(webserver_id=webserver_id, safe=True)
    # then
    assert isinstance(result, Request)
    assert result.webserver_id == webserver_id


async def test_get_repository_for_should_return_valid_repository(async_object_bound_repository, host):
    # given
    model = Webserver
    # when
    result = async_object_bound_repository.get_repository_for(model)
    # then
    assert isinstance(result, AsyncObjectBoundRepository)
    assert result.statement_generator.bound_object_id == host.id
    assert result.statement_generator.cls == model
