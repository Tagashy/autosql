import pytest
from sqlalchemy.ext.asyncio import AsyncSession

from sqlgen import AsyncConstrainedRepository
from sqlgen.joins import Constraint
from sqlgen.exc import ConstraintNotSafe, DirectConstraintNotSafe
from test_data.factories import HostFactory
from test_data.models import Request, Host, Webserver, Project, VulnerabilityInstance

MODULE_PACKAGE = "sqlgen.repository.impl.async_constrained"


@pytest.fixture()
def host():
    return HostFactory()


@pytest.fixture()
def async_constrained_repository(mocker, host):
    class Repository(AsyncConstrainedRepository):
        cls = Request
        bound_model = Host

    return Repository(mocker.Mock(spec=AsyncSession), host_id=host.id)


async def test_create_should_return_created_object(async_constrained_repository, host, mocker):
    # given
    get_repository_for_patch = mocker.patch.object(async_constrained_repository, "get_repository_for")
    get_repository_for_patch.return_value.get_by_id = mocker.AsyncMock(return_value=True)
    # when
    result = await async_constrained_repository.create(webserver_id=host.webservers[0].id, flag=True)
    # then
    assert isinstance(result, Request)
    assert result.flag is True


async def test_create_should_raise_direct_constraint_not_safe_inner_calls(async_constrained_repository, host, mocker):
    # given
    get_repository_for_patch = mocker.patch.object(async_constrained_repository, "get_repository_for")
    get_repository_for_patch.return_value.get_by_id = mocker.AsyncMock(return_value=None)
    webserver_id = host.webservers[0].id
    # when
    with pytest.raises(DirectConstraintNotSafe) as exc_info:
        await async_constrained_repository.create(webserver_id=webserver_id, flag=True)
    # then
    assert exc_info.value.model == Webserver
    assert exc_info.value.foreign_key_value == webserver_id


async def test_create_should_raise_direct_constraint_not_safe(host, mocker, async_constrained_repository):
    # given
    constraint = Constraint(Host.project_id, Project)
    webserver_id = host.webservers[0].id
    create_patch = mocker.patch("sqlgen.repository.impl.asynchronous.AsyncRepository.create",
                                side_effect=DirectConstraintNotSafe(constraint, webserver_id, Webserver))
    handle_direct_constraint_not_safe = mocker.patch.object(async_constrained_repository,
                                                            "handle_direct_constraint_not_safe")

    # when
    await async_constrained_repository.create(webserver_id=webserver_id, flag=True)
    # then
    create_patch.assert_called_once_with(webserver_id=webserver_id, flag=True, safe_constraints=[])
    handle_direct_constraint_not_safe.assert_called_once_with(Webserver, webserver_id, constraint, safe_constraints=[],
                                                              webserver_id=webserver_id, flag=True)


async def test_create_should_raise_constraint_not_safe(host, mocker, async_constrained_repository):
    # given
    constraint = Constraint(Host.project_id, Project)
    create_patch = mocker.patch("sqlgen.repository.impl.asynchronous.AsyncRepository.create",
                                side_effect=ConstraintNotSafe(constraint, []))
    handle_constraint_not_safe_patch = mocker.patch.object(async_constrained_repository, "handle_constraint_not_safe")
    webserver_id = host.webservers[0].id

    # when
    await async_constrained_repository.create(webserver_id=webserver_id, flag=True)
    # then
    create_patch.assert_called_once_with(webserver_id=webserver_id, flag=True, safe_constraints=[])
    handle_constraint_not_safe_patch.assert_called_once_with(constraint, [], safe_constraints=[],
                                                             webserver_id=webserver_id, flag=True)


async def test_handle_direct_constraint_not_safe_should_check_for_safety_of_relation(mocker, host,
                                                                                     async_constrained_repository):
    # given
    constraint = Constraint(Host.project_id, Project)
    get_repository_for_patch = mocker.patch.object(async_constrained_repository, "get_repository_for")
    get_repository_for_patch.return_value.get_by_id = mocker.AsyncMock(return_value=True)
    create_patch = mocker.patch.object(async_constrained_repository, "create")
    webserver_id = host.webservers[0].id

    # when
    result = await async_constrained_repository.handle_direct_constraint_not_safe(Webserver, webserver_id, constraint,
                                                                                  safe_constraints=[],
                                                                                  webserver_id=webserver_id, flag=True)
    # then
    create_patch.assert_called_once_with(webserver_id=webserver_id, flag=True, safe_constraints=[constraint])
    assert result == create_patch.return_value
    get_repository_for_patch.assert_called_once_with(Webserver)
    get_repository_for_patch.return_value.get_by_id.assert_called_once_with(webserver_id)


async def test_handle_direct_constraint_not_safe_should_raise_if_constraint_failed(mocker, host,
                                                                                   async_constrained_repository):
    # given
    constraint = Constraint(Host.project_id, Project)
    get_repository_for_patch = mocker.patch.object(async_constrained_repository, "get_repository_for")
    get_repository_for_patch.return_value.get_by_id = mocker.AsyncMock(return_value=None)
    create_patch = mocker.patch.object(async_constrained_repository, "create")
    webserver_id = host.webservers[0].id

    # then
    with pytest.raises(ZeroDivisionError):
        # given
        # we need to be in the context of an exception as this is an exception handler we are testing
        try:
            1 / 0
        except ZeroDivisionError:
            # when
            await async_constrained_repository.handle_direct_constraint_not_safe(Webserver, webserver_id, constraint,
                                                                                 safe_constraints=[],
                                                                                 webserver_id=webserver_id, flag=True)


async def test_handle_constraint_not_safe_should_check_for_safety_of_relations(mocker, host,
                                                                               async_constrained_repository):
    # given
    constraint = Constraint(Host.project_id, Project)
    constraint2 = Constraint(Webserver.host_id, Host, _bound_object_id=1)
    constraint3 = Constraint(VulnerabilityInstance.request_id, Request, _bound_object_id=2)
    get_repository_for_patch = mocker.patch.object(async_constrained_repository, "get_repository_for")
    get_repository_for_patch.return_value.get_by_id = mocker.AsyncMock(return_value=True)
    create_patch = mocker.patch.object(async_constrained_repository, "create")
    webserver_id = host.webservers[0].id

    # when
    result = await async_constrained_repository.handle_constraint_not_safe(constraint, [constraint2, constraint3],
                                                                           safe_constraints=[],
                                                                           webserver_id=webserver_id, flag=True)
    # then
    create_patch.assert_called_once_with(webserver_id=webserver_id, flag=True, safe_constraints=[constraint])
    assert result == create_patch.return_value
    get_repository_for_patch.assert_any_call(Host)
    get_repository_for_patch.assert_any_call(Request)
    get_repository_for_patch.return_value.get_by_id.assert_any_call(1)
    get_repository_for_patch.return_value.get_by_id.assert_any_call(2)


async def test_handle_constraint_not_safe_should_raise_if_constraint_is_not_safe(mocker, host,
                                                                                 async_constrained_repository):
    # given
    constraint = Constraint(Host.project_id, Project)
    constraint2 = Constraint(Webserver.host_id, Host, _bound_object_id=1)
    constraint3 = Constraint(VulnerabilityInstance.request_id, Request, _bound_object_id=2)
    get_repository_for_patch = mocker.patch.object(async_constrained_repository, "get_repository_for")
    get_repository_for_patch.return_value.get_by_id = mocker.AsyncMock(return_value=None)
    create_patch = mocker.patch.object(async_constrained_repository, "create")
    webserver_id = host.webservers[0].id

    # then
    with pytest.raises(ZeroDivisionError):
        # given
        # we need to be in the context of an exception as this is an exception handler we are testing
        try:
            1 / 0
        except ZeroDivisionError:
            # when
            await async_constrained_repository.handle_constraint_not_safe(constraint, [constraint2, constraint3],
                                                                          safe_constraints=[],
                                                                          webserver_id=webserver_id, flag=True)


async def test_get_repository_for_should_return_valid_repository(async_constrained_repository, host):
    # given
    model = Webserver
    # when
    result = async_constrained_repository.get_repository_for(model)
    # then
    assert isinstance(result, AsyncConstrainedRepository)
    assert result.statement_generator.constraints == [Constraint(joined_column=Webserver.host_id,
                                                                 joined_model=Host,
                                                                 joins=[],
                                                                 _bound_object_id=host.id)]
    assert result.statement_generator.cls == model


async def test_get_constraints_for_should_return_a_list_of_constraint_for_given_model(mocker, host):
    # given
    class Repository(AsyncConstrainedRepository):
        cls = Request
        bound_models = [Host, Project]

    repository = Repository(mocker.Mock(spec=AsyncSession), host_id=host.id, project_id=host.project_id)
    model = Webserver
    build_parent_constraint_patch = mocker.patch(f"{MODULE_PACKAGE}.build_parent_constraint")
    # when
    result = repository.get_constraints_for(model)
    # then
    build_parent_constraint_patch.assert_any_call(Constraint(joined_column=Host.project_id,
                                                             joined_model=Project,
                                                             joins=[Request.webserver, Webserver.host],
                                                             _bound_object_id=host.project_id), Webserver)
    build_parent_constraint_patch.assert_any_call(Constraint(joined_column=Webserver.host_id,
                                                             joined_model=Host,
                                                             joins=[Request.webserver],
                                                             _bound_object_id=host.id), Webserver)


def test_async_constrained_repository_should_raise_if_instantiated_directly(mocker):
    with pytest.raises(ValueError) as exc_info:
        AsyncConstrainedRepository(mocker.Mock())
    assert exc_info.value.args == ("Cannot instantiate AsyncConstrainedRepository directly",)
