from uuid import uuid4

import pytest
from sqlalchemy.orm import Session

from sqlgen import SynchronousRepository
from test_data.factories import HostFactory
from test_data.models import Host
from tests.matchers import InstanceOfWith


@pytest.fixture()
def synchronous_repository(mocker):
    class Repository(SynchronousRepository):
        cls = Host

    return Repository(mocker.Mock(spec=Session))


def test_synchronous_repository_should_raise_if_instantiated_directly(mocker):
    with pytest.raises(ValueError) as exc_info:
        SynchronousRepository(mocker.Mock(spec=Session), uuid4())
    # then
    assert exc_info.value.args == ("Cannot instantiate AsyncRepository directly",)


def test_get_by_should_execute_statement_returned_by_statement_generator(synchronous_repository, mocker):
    # given
    project_id = uuid4()
    get_by_patch = mocker.patch.object(synchronous_repository.statement_generator, "get_by")
    one_or_none_patch = mocker.patch.object(synchronous_repository.statement_executor, "one_or_none")
    filter_ = Host.project_id.in_([project_id])
    # when
    synchronous_repository.get_by(filter_, name="test")
    # then
    get_by_patch.assert_called_once_with(filter_, name="test", load_all=False, options=None,
                                         property_name=None)
    one_or_none_patch.assert_called_once_with(get_by_patch.return_value)


def test_take_by_should_execute_statement_returned_by_statement_generator(synchronous_repository, mocker):
    # given
    project_id = uuid4()
    get_by_patch = mocker.patch.object(synchronous_repository.statement_generator, "get_by")
    one_patch = mocker.patch.object(synchronous_repository.statement_executor, "one")
    filter_ = Host.project_id.in_([project_id])
    # when
    synchronous_repository.take_by(filter_, name="test")
    # then
    get_by_patch.assert_called_once_with(filter_, name="test", load_all=False, options=None,
                                         property_name=None)
    one_patch.assert_called_once_with(get_by_patch.return_value)


def test_get_by_id_should_execute_statement_returned_by_statement_generator(synchronous_repository, mocker):
    # given
    project_id = uuid4()
    get_by_patch = mocker.patch.object(synchronous_repository.statement_generator, "get_by")
    one_or_none_patch = mocker.patch.object(synchronous_repository.statement_executor, "one_or_none")
    filter_ = Host.project_id.in_([project_id])
    # when
    synchronous_repository.get_by_id(project_id, filter_, name="test")
    # then
    get_by_patch.assert_called_once_with(filter_, name="test", id=project_id, load_all=False, options=None,
                                         property_name=None)
    one_or_none_patch.assert_called_once_with(get_by_patch.return_value)


def test_take_by_id_should_execute_statement_returned_by_statement_generator(synchronous_repository, mocker):
    # given
    project_id = uuid4()
    get_by_patch = mocker.patch.object(synchronous_repository.statement_generator, "get_by")
    one_patch = mocker.patch.object(synchronous_repository.statement_executor, "one")
    filter_ = Host.project_id.in_([project_id])
    # when
    synchronous_repository.take_by_id(project_id, filter_, name="test")
    # then
    get_by_patch.assert_called_once_with(filter_, name="test", id=project_id, load_all=False, options=None,
                                         property_name=None)
    one_patch.assert_called_once_with(get_by_patch.return_value)


def test_get_all_by_should_execute_statement_returned_by_statement_generator(synchronous_repository, mocker):
    # given
    project_id = uuid4()
    get_by_patch = mocker.patch.object(synchronous_repository.statement_generator, "get_by")
    all_patch = mocker.patch.object(synchronous_repository.statement_executor, "all")
    filter_ = Host.project_id.in_([project_id])
    # when
    synchronous_repository.get_all_by(filter_, name="test")
    # then
    get_by_patch.assert_called_once_with(filter_, name="test", options=None)
    all_patch.assert_called_once_with(get_by_patch.return_value)


def test_get_all_should_execute_statement_returned_by_statement_generator(synchronous_repository, mocker):
    # given
    get_by_patch = mocker.patch.object(synchronous_repository.statement_generator, "get_by")
    all_patch = mocker.patch.object(synchronous_repository.statement_executor, "all")
    # when
    synchronous_repository.get_all()
    # then
    get_by_patch.assert_called_once_with(options=None)
    all_patch.assert_called_once_with(get_by_patch.return_value)


def test_create_should_create_object_with(synchronous_repository, mocker):
    # given
    project_id = uuid4()
    store_patch = mocker.patch.object(synchronous_repository.statement_executor, "store")
    # when
    obj = synchronous_repository.create(name="toto", project_id=project_id)
    # then
    store_patch.assert_called_once_with(
        InstanceOfWith(Host, name="toto", project_id=project_id))
    assert isinstance(obj, Host)


def test_update_should_set_properties_and_synchronize(synchronous_repository, mocker):
    # given
    host = HostFactory()
    synchronize_patch = mocker.patch.object(synchronous_repository, "synchronize")
    # when
    synchronous_repository.update(host, name="toto")
    # then
    assert host.name == "toto"
    synchronize_patch.assert_called_once_with()


def test_update_should_commit(synchronous_repository, mocker):
    # given
    host = HostFactory()
    save_patch = mocker.patch.object(synchronous_repository, "save")
    # when
    synchronous_repository.update(host, save=True)
    # then
    save_patch.assert_called_once_with()


def test_update_should_load_object_and_update_it(synchronous_repository, mocker):
    # given
    host = HostFactory()
    take_by_id_patch = mocker.patch.object(synchronous_repository, "take_by_id", return_value=host)
    # when
    synchronous_repository.update(host.id, name="toto")
    # then
    assert host.name == "toto"
    take_by_id_patch.assert_called_once_with(host.id, load_all=True)


def test_synchronize_should_call_statement_executor_synchronize(synchronous_repository, mocker):
    # given
    synchronize_patch = mocker.patch.object(synchronous_repository.statement_executor, "synchronize")
    # when
    synchronous_repository.synchronize()
    # then
    synchronize_patch.assert_called_once_with()


def test_save_should_call_statement_executor_save(synchronous_repository, mocker):
    # given
    save_patch = mocker.patch.object(synchronous_repository.statement_executor, "save")
    # when
    synchronous_repository.save()
    # then
    save_patch.assert_called_once_with()


def test_restore_should_call_statement_executor_restore(synchronous_repository, mocker):
    # given
    restore_patch = mocker.patch.object(synchronous_repository.statement_executor, "restore")
    # when
    synchronous_repository.restore()
    # then
    restore_patch.assert_called_once_with()
