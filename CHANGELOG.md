# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

### Added

- 

### Changed

- 

### Removed

- 

## [1.0.0] - 2024-03-22

### Changed

- child class of repository don't need to specify generate=False anymore, any child class without a defined "cls" class variable will be considered as a "mixin" class and no specific statement generator will be generated

### Removed

- all metaclass replaced by init_subclass making it easier to code and extend outside of this library.

## [0.7.0] - 2024-03-19

### Added

- Function helpers to generate repository class without needing to define a class. 
- Handle one to many relation for model join by joining the primary key of the many side.

## [0.6.0] - 2024-03-07

### Added

- ConstrainedRepository, allowing to have multiple bound_model and check the safety of each bound_model on create and get

### Changed

- AsyncExecutor.scalars auto uniquify result


## [0.5.0] - 2024-03-04

### Added

- DatabaseRepository.create allow the creation of object
- ObjectBoundStatementGenerator.create
  - Add foreign_key automatically if bound object is directly related (e.g. Host to Project in test_data)
  - Validate that foreign_key value is bounded to bound_model if bound_model is not directly related (e.g. Request of Webserver to Project in test_data). 
- automatic documentation generation

### Changed

- better typing overall
- ObjectBoundStatementGenerator.get_by: change joins logic to use the last join as a filter as it is expected to be a column (e.g. Host.project_id), also faster query as they don't join on bound_column. e.g. Evolved from "Select(Request).join(Request.webserver).join(Webserver.host).join(Host.project).filter(Project.id==self.bound_object_id)" to "Select(Request).join(Request.webserver).join(Webserver.host).filter(Host.project_id==self.bound_object_id)"

### Removed

- primary key property from ObjectBoundStatementGenerator

## [0.4.0] - 2024-03-01

### Added

- typing for AsyncObjectBoundRepository.bound_object_id
- Handle Class Heritage for DatabaseRepository and ObjectBoundRepository, mandatory class properties


## [0.3.0] - 2024-02-29

### Added

- Changelog
- public pypi package
- basic documentation for the package
- Support for AsyncRepository
- Support AsyncObjectBoundRepository
