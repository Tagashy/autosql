from easyfactory import make_factory_for

from test_data.models import Project, Request, Host, Webserver

ProjectFactory = make_factory_for(Project)
HostFactory = make_factory_for(Host)
WebserverFactory = make_factory_for(Webserver)
RequestFactory = make_factory_for(Request)
