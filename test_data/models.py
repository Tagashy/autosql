from typing import Annotated
from uuid import UUID, uuid4

from sqlalchemy import ForeignKey
from sqlalchemy.orm import DeclarativeBase, mapped_column, Mapped, relationship

UUID_PK = Annotated[UUID, mapped_column(primary_key=True)]
HOST_FK = Annotated[UUID, mapped_column(ForeignKey("host.id"))]
PROJECT_FK = Annotated[UUID, mapped_column(ForeignKey("project.id"))]
REQUEST_FK = Annotated[UUID, mapped_column(ForeignKey("request.id"))]
VULNERABILITY_CLASS_FK = Annotated[UUID, mapped_column(ForeignKey("vulnerability_class.id"))]
WEBSERVER_FK = Annotated[UUID, mapped_column(ForeignKey("webserver.id"))]


class Base(DeclarativeBase):
    id: Mapped[UUID_PK] = mapped_column(default=uuid4)


class User(Base):
    __tablename__ = "user"

    name: Mapped[str]
    hashed_password: Mapped[bytes]
    disabled: Mapped[bool] = mapped_column(default=False)


class VulnerabilityClass(Base):
    __tablename__ = "vulnerability_class"

    name: Mapped[str]
    description: Mapped[str]
    consequences: Mapped[str]
    recommendation: Mapped[str]
    impact_description: Mapped[str]
    # WARNING: This relationship will return all the instances REGARDLESS of project
    instances: Mapped[list["VulnerabilityInstance"]] = relationship(back_populates="vulnerability_class",
                                                                    cascade="all, delete-orphan")

    def __hash__(self):
        return hash((self.name, self.description, self.consequences, self.recommendation, self.impact_description,
                     self.impact, self.exploitability, self.correction_difficulty))


class VulnerabilityInstance(Base):
    __tablename__ = "vulnerability_instance"

    vulnerability_class_id: Mapped[VULNERABILITY_CLASS_FK]

    request_id: Mapped[REQUEST_FK]

    request: Mapped["Request"] = relationship(back_populates="vulnerabilities")

    vulnerability_class: Mapped["VulnerabilityClass"] = relationship(back_populates="instances")


class Request(Base):
    __tablename__ = "request"

    flag: Mapped[bool] = False

    webserver_id: Mapped[WEBSERVER_FK]
    webserver: Mapped["Webserver"] = relationship(back_populates="requests")
    vulnerabilities: Mapped[list["VulnerabilityInstance"]] = relationship(back_populates="request",
                                                                          cascade="all, delete-orphan")


class WebserverDiscovery(Base):
    __tablename__ = "webserver_discovery"

    webserver_id: Mapped[WEBSERVER_FK]

    framework: Mapped[str]

    webserver: Mapped["Webserver"] = relationship(back_populates="discovery", single_parent=True)


class Webserver(Base):
    __tablename__ = "webserver"
    name: Mapped[str]
    host_id: Mapped[HOST_FK]
    host: Mapped["Host"] = relationship(back_populates="webservers")
    requests: Mapped[list["Request"]] = relationship(back_populates="webserver", cascade="all, delete-orphan")
    discovery: Mapped["WebserverDiscovery"] = relationship(back_populates="webserver",
                                                           cascade="all, delete-orphan",
                                                           single_parent=True)


class Host(Base):
    __tablename__ = "host"
    name: Mapped[str]
    project_id: Mapped[PROJECT_FK]
    project: Mapped["Project"] = relationship(back_populates="hosts")
    webservers: Mapped[list["Webserver"]] = relationship(back_populates="host", cascade="all, delete-orphan")


class Project(Base):
    __tablename__ = "project"
    hosts: Mapped[list["Host"]] = relationship(back_populates="project", cascade="all, delete-orphan")
